import { Component, Injectable, Input, ViewEncapsulation} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Injectable({
  providedIn: 'root'
})
@Component({
  selector: 'app-alert-service',
  templateUrl: './alert-controller.service.html',
  styleUrls: ['./alert-controller.service.css'],
  encapsulation: ViewEncapsulation.None
})
// tslint:disable-next-line:component-class-suffix
export class AlertController {
  @Input() public title: string;
  @Input() public message: string;
  @Input() public isAlert: boolean;
  private canShow = true;

  constructor(private modalService: NgbModal, public activeModal: NgbActiveModal) {

  }

  public showAlert(title: string, message: string, acceptHandler: any): void {
    this.showModal(title, message, true, acceptHandler);
  }

  public showConfirm(title: string, message: string, acceptHandler: any, cancelHandler: any): void {
    this.showModal(title, message, false, acceptHandler, cancelHandler);
  }

  private showModal(title: string, message: string, isAlert: boolean, acceptHandler: any, cancelHandler?: any): void {
    if (!this.canShow) {
      return;
    }

    const modalRef = this.modalService.open(AlertController, { windowClass: 'dark-modal', backdrop: 'static', centered: true });
    modalRef.componentInstance.isAlert = isAlert;
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.message = message;

    this.canShow = false;
    modalRef.result.then(() => { // close, called on accpt button click
      this.canShow = true;
      acceptHandler();
    }, () => { // dismiss, called on cancel button click
      this.canShow = true;
      cancelHandler();
    });
  }
}
