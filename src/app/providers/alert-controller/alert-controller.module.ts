import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AlertController} from './alert-controller.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  entryComponents: [
    AlertController
  ],
  declarations: [
    AlertController
  ],
  imports: [
    CommonModule,
    TranslateModule
  ],
  providers: [
    NgbActiveModal
  ]
})
export class AlertControllerModule { }
