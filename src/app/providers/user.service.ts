import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParameterCodec, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {EncrdecrService} from './encrdecr.service';
import {Router} from '@angular/router';
import {map, retry} from 'rxjs/operators';
import {BehaviorSubject, empty, Observable} from 'rxjs';

class CustomEncoder implements HttpParameterCodec {
  encodeKey(key: string): string {
    return encodeURIComponent(key);
  }

  encodeValue(value: string): string {
    return encodeURIComponent(value);
  }

  decodeKey(key: string): string {
    return decodeURIComponent(key);
  }

  decodeValue(value: string): string {
    return decodeURIComponent(value);
  }
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private encrypt: EncrdecrService,
    private http: HttpClient,
    private route: Router) {
  }

  // Private
  private basePath = environment.urlApiManager;
  public initialTokenValue = -9999;
  // Public
  loginActiveSubject = new BehaviorSubject<boolean>(false);
  tokenDurationSubject = new BehaviorSubject<number>(this.initialTokenValue);
  isUserLoggedIn = false;

  getToken(username, password) {
    const headers = new HttpHeaders({
      Authorization: environment.Authorization,
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    const httpBody = new HttpParams({encoder: new CustomEncoder()})
      .set('grant_type', 'password')
      .set('username', username)
      .set('password', password)
      .set('scope', this.getScopeParam());

    return this.http.post(`${this.basePath}token`, httpBody, {headers}).pipe(
      map((res) => {
          this.tokenDurationSubject.next(res['expires_in']);
          return res;
        }
      ));
  }

  private getRandom() {
    return (Math.floor(Math.random() * 1000) + 1000).toString();
  }

  private getScopeParam(): string {
    return 'device_' + Date.now() + this.getRandom();
  }

  private getTokenWithoutUser() {
    const headers = new HttpHeaders({
      Authorization: environment.Authorization,
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    const httpBody = new HttpParams()
      .set('grant_type', 'client_credentials')
      .set('scope', this.getScopeParam());

    return this.http.post(`${this.basePath}token`, httpBody, {headers}).pipe(
      map((res) => {
          this.tokenDurationSubject.next(res['expires_in']);
          return res;
        }
      ));
  }

  getHeader(): HttpHeaders {
    const token = (sessionStorage.getItem('_token')) ? JSON.parse(sessionStorage.getItem('_token')) : {token_type: '', access_token: ''};

    return new HttpHeaders({
      Authorization: `${token.token_type} ${token.access_token}`
    });
  }

  recoverPass(body) {

    if (sessionStorage.getItem('_token')) {
      return this.http.post(`${this.basePath}ldap/user/recoverpassword`, body, {headers: this.getHeader()});
    } else {
      console.warn('No existe _token');
      return this.http.post(`${this.basePath}ldap/user/recoverpassword`, body);
    }
  }

  setPass(body) {

    if (sessionStorage.getItem('_token')) {
      return this.http.post(`${this.basePath}ldap/user/setnewpass`, body, {headers: this.getHeader()});
    } else {
      console.warn('No existe _token');
      return this.http.post(`${this.basePath}ldap/user/setnewpass`, body);
    }
  }

  changeDefaultPass(body) {

    if (sessionStorage.getItem('_token')) {
      return this.http.post(`${this.basePath}ldap/user/changepassword`, body, {headers: this.getHeader()});
    } else {
      console.warn('No existe _token');
      return this.http.post(`${this.basePath}ldap/user/changepassword`, body);
    }
  }

  logout(sessionExpired?) {
    sessionStorage.removeItem('_token');
    sessionStorage.removeItem('currentUser');
    // Emite close session
    this.loginActiveSubject.next(false);
    if (sessionExpired) {
      this.route.navigate(['login']);
    }
  }

  getUserInfo() {

    return this.getRequest(`ldap/user/control`);

  }

  uploadPhoto(body: any): Observable<any> {
    if (sessionStorage.getItem('_token')) {
      return this.http.post(`${this.basePath}ldap/photo/upload`, body, {headers: this.getHeader()});
    } else {
      return this.http.post(`${this.basePath}ldap/photo/upload`, body);
    }
  }

  getUser() {
    return (sessionStorage.getItem('currentUser') && sessionStorage.getItem('currentUser').length > 0) ?
      JSON.parse(sessionStorage.getItem('currentUser')) : false;
  }

  isUserLogged() {
    if (this.getUser()) {
      this.loginActiveSubject.next(true);
    }
    return this.getUser();
  }

  setUserInfo(user: any) {
    this.isUserLoggedIn = true;
    this.loginActiveSubject.next(true);
    sessionStorage.setItem('currentUser', JSON.stringify(user));
  }

  private getRequest(servicePath: string, params?: HttpParams): Observable<any> {
    if (sessionStorage.getItem('_token')) {
      return this.http.get(`${this.basePath}${servicePath}`, {headers: this.getHeader(), params});
    } else {
      return empty();
    }
  }

  revokeToken() {
    if (sessionStorage.getItem('_token')) {
      const headers = new HttpHeaders({
        Authorization: environment.Authorization,
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: 'application/json, text/plain, */*'
      });

      return this.http.post(`${this.basePath}revoke`,
        `token=${JSON.parse(sessionStorage.getItem('_token')).access_token}`, {headers}).pipe(
        retry(3)
      );
    } else {
      return empty();
    }

  }

  getTokenIfNoExists(): Promise<any> {
    return new Promise<string>((resolve, reject) => {
      if (!sessionStorage.getItem('_token')) {
        this.getTokenWithoutUser().subscribe(
          (res) => {
            resolve();
          },
          (error) => {
            console.log(error);
            reject();
          }
        );
      } else {
        resolve();
      }
    });
  }


}
