import {Injectable} from '@angular/core';

// tslint:disable-next-line:ban-types
declare var gtag: Function;

@Injectable()
export class AnalyticsService {
  private googleAnalyticsKey = 'UA-162267765-1';

  constructor() {
  }

  public init() {
    try {

      const script1 = document.createElement('script');
      script1.async = true;
      script1.src = 'https://www.googletagmanager.com/gtag/js?id=' + this.googleAnalyticsKey;
      document.head.appendChild(script1);

      const script2 = document.createElement('script');
      script2.innerHTML = `
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());
                gtag('config', '` + this.googleAnalyticsKey + `');
              `;
      document.head.appendChild(script2);
    } catch (ex) {
      console.error('Error appending google analytics');
      console.error(ex);
    }
  }

  public showScreen(screen) {
    gtag('event', 'view', {
      event_category: 'page_view',
      event_label: screen
    });
  }

  public registerClick(name) {
    gtag('event', 'click', {
      event_category: 'user_interaction',
      event_label: name
    });
  }

  public registerUser(userId: string) {
    // console.log('registerUser ' + userId);

    gtag('event', 'click', {
      event_category: 'login_user',
      event_label: 'login',
      value: btoa(userId)
    });

  }

  public registerError(error: any, method?: string) {
    let finalDescription = (method) ? 'method: ' + method + ' -' : '';
    const url = (error.url) ? ' url: ' + error.url + ' -' : '';
    finalDescription += url;
    const message = (error.message) ? ' message: ' + error.message : '';
    finalDescription += message;

    // console.log(finalDescription);

    gtag('event', 'exception', {
      description: (finalDescription !== '') ? finalDescription : 'generic_error'
    });
  }

}
