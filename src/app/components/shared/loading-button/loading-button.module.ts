import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {LoadingButtonComponent} from './loading-button.component';

@NgModule({
  entryComponents: [
    LoadingButtonComponent
  ],
  declarations: [
    LoadingButtonComponent
  ],
  imports: [
    CommonModule,
    TranslateModule.forChild(),
  ],
  exports: [
    LoadingButtonComponent
  ]
})
export class LoadingButtonModule { }
