import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RecoverPassComponent} from './recover-pass.component';

const routes: Routes = [
  {
    path: '',
    component: RecoverPassComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecoverPassRoutingModule { }
