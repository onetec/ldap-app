import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertController} from '../../providers/alert-controller/alert-controller.service';
import {Router} from '@angular/router';
import {IdentityDocumentValidators} from '../../validators/identity-document-validators';
import {UserService} from '../../providers/user.service';

const TIMER_INTERVAL = 300000;

@Component({
  selector: 'app-recover-pass',
  templateUrl: './recover-pass.component.html',
  styleUrls: ['./recover-pass.component.scss']
})
export class RecoverPassComponent implements OnDestroy, OnInit {

  loading = false;
  formGroup: FormGroup;
  timer: any = null;
  requestDone = false;

  constructor(private userService: UserService,
              private fb: FormBuilder,
              private alertController: AlertController,
              public router: Router) {

  }

  ngOnInit() {
    this.formGroup = this.fb.group({
      username: ['', Validators.required],
      document: ['', [Validators.required, IdentityDocumentValidators.evaluateValidDocument]],
    });
  }

  ngOnDestroy() {
    this.stopInterval();
  }

  async sendData() {


    await this.userService.getTokenIfNoExists();

    const userSplited = this.formGroup.get('username').value.split('@');
    const body = {
      user: userSplited[0],
      document: btoa(this.formGroup.get('document').value.toUpperCase()),
    };

    if (!this.requestDone) {
      this.requestDone = true;
      if (this.timer === null) {// only start timer if there is not another one
        this.avoidDuplicateRequest();
      }
      this.loading = true;
      this.userService.recoverPass(body).subscribe(
        (res: any) => {
          this.loading = false;

          if (res.error === false) {
            this.showResponseMessage(false,
              'recover-pass.request-ok-message');
          } else {
            this.stopInterval();
            this.showResponseMessage(true, res.message);
          }

        },
        (error) => {
          this.loading = false;
          this.requestDone = false;
          this.stopInterval();
          this.showResponseMessage(true);
        }
      );

    } else {
      this.showResponseMessage(true, 'recover-pass.request-wait-message');
    }

  }

  private avoidDuplicateRequest() {
    this.timer = setInterval(() => {
      this.requestDone = false;
      this.stopInterval();
      // console.log('interval finished');
    }, TIMER_INTERVAL);
  }

  private stopInterval() {
    clearInterval(this.timer);
    this.timer = null;
    this.requestDone = false;
  }

  private showResponseMessage(error?, message?) {

    this.alertController.showAlert((error) ? 'Error' : 'Info',
      (message) ? message : 'error-access-message', () => {
      });
  }
}
