import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {EncrdecrService} from '../../providers/encrdecr.service';
import {UserService} from '../../providers/user.service';
import {AlertController} from '../../providers/alert-controller/alert-controller.service';

@Component({
  selector: 'app-change-pass',
  templateUrl: './change-pass.component.html',
  styleUrls: ['./change-pass.component.scss']
})
export class ChangePassComponent implements OnInit, OnDestroy {

  loading = false;
  formGroup: FormGroup;
  showPassword = false;
  showRePassword = false;
  urlUser: string;
  urlToken: string;
  private sub: any;

  constructor(private route: ActivatedRoute,
              private fb: FormBuilder,
              private encrypt: EncrdecrService,
              public router: Router,
              private userService: UserService,
              private alertController: AlertController) {
  }

  ngOnInit() {
    this.sub = this.route.queryParams.subscribe(params => {
      this.urlUser = params['user'];
      this.urlToken = params['token'];
    });

    this.formGroup = this.fb.group({
      newPassword: ['', [Validators.required, Validators.minLength(6)]],
      rePassword: ['', [Validators.required, Validators.minLength(6)]]
    }, {
      validators: this.passwordMatchValidator
    });

  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  showPass() {
    this.showPassword = !this.showPassword;
  }

  showRePass() {
    this.showRePassword = !this.showRePassword;
  }

  passwordMatchValidator(g: FormGroup) {
    return g.get('newPassword').value === g.get('rePassword').value
      ? null : {notSame: true};
  }

  async sendData() {

    await this.userService.getTokenIfNoExists();

    const body = {
      user: this.urlUser,
      token: decodeURIComponent(this.urlToken).split(' ').join('+'), // decode token
      newPass: this.encrypt.setEncrypt(this.formGroup.get('newPassword').value)
    };

    this.userService.setPass(body).subscribe(
      (res: any) => {

        if (res.error === false) {

          this.userService.getToken(this.urlUser, this.formGroup.get('newPassword').value).subscribe(
            (token) => {
              sessionStorage.setItem('_token', JSON.stringify(token));

              this.userService.getUserInfo().subscribe(
                (loginResponse: any) => {
                  if (loginResponse.error) {
                    this.router.navigate(['login']);

                  } else {

                    this.alertController.showAlert('Info',
                      'pass-saved-message', () => {
                        loginResponse.data.uid = this.urlUser;
                        this.userService.setUserInfo(loginResponse);
                        this.router.navigate(['profile']);
                      });
                  }
                },
                (error) => {
                  console.log(error);
                  this.showResponseMessage();
                }
              );

            },
            (error) => {
              console.log(error);
              this.userService.logout();
            }
          );
        } else {
          this.showResponseMessage(true, res.message);
        }
      },
      (error) => {
        this.showResponseMessage();
      }
    );
  }

  private showResponseMessage(error?, message?) {

    this.alertController.showAlert((error) ? 'Error' : 'Info',
      (message) ? message : 'error-access-message', () => {
      });
  }
}
