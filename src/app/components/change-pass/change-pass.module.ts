import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ChangePassRoutingModule} from './change-pass-routing.module';


@NgModule({
  imports: [
    CommonModule,
    ChangePassRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forChild()
  ],
})
export class ChangePassModule { }
