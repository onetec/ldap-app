import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../providers/user.service';
import {AlertController} from '../../providers/alert-controller/alert-controller.service';
import {Router} from '@angular/router';

const INVALID_CREDENTIALS_ERROR = 'invalid_grant';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loading = false;
  formGroup: FormGroup;
  showPassword = false;

  constructor(private userService: UserService,
              private fb: FormBuilder,
              private alertController: AlertController,
              public router: Router) {

  }

  ngOnInit() {
    if (this.userService.getUser()) {
      this.router.navigate(['profile']);
    }
    this.formGroup = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  showPass() {
    this.showPassword = !this.showPassword;
  }

  sendData() {

    this.loading = true;

    const username = this.formGroup.get('username').value;
    const password = this.formGroup.get('password').value;

    this.userService.getToken(username, password).subscribe(
      (res) => {
        sessionStorage.setItem('_token', JSON.stringify(res));
        this.callLogin(username, password);
      },
      (error) => {
        if (this.areInvalidCredentials(error)) {
          this.alertController.showAlert('Info', 'invalidCredentials', () => {
          });
        }
        console.log(error);
        this.userService.logout();
        this.loading = false;
      }
    );
  }

  private callLogin(username, password) {
    this.userService.getUserInfo().subscribe(
      (res: any) => {
        this.loading = false;
        if (res.error) {
          this.alertController.showAlert('Error', 'error-access-message', () => {
          });
        } else {
          if (res.firstTime) {
            this.router.navigate(['change-default-pass', [username, password]]);
          } else {
            this.userService.setUserInfo(res);
            this.router.navigate(['profile']);
          }
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  private areInvalidCredentials(response: any): boolean {
    return response && response.error && response.error.error === INVALID_CREDENTIALS_ERROR;
  }
}
