import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ChangeDefaultPassComponent} from './change-default-pass.component';

const routes: Routes = [
  {
    path: '',
    component: ChangeDefaultPassComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChangeDefaultPassRoutingModule { }
