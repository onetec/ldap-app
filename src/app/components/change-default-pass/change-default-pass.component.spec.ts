import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeDefaultPassComponent } from './change-default-pass.component';

describe('ChangeDefaultPassComponent', () => {
  let component: ChangeDefaultPassComponent;
  let fixture: ComponentFixture<ChangeDefaultPassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeDefaultPassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeDefaultPassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
