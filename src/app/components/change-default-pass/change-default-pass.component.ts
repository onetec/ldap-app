import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {EncrdecrService} from '../../providers/encrdecr.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../providers/user.service';
import {AlertController} from '../../providers/alert-controller/alert-controller.service';

@Component({
  selector: 'app-change-default-pass',
  templateUrl: './change-default-pass.component.html',
  styleUrls: ['./change-default-pass.component.scss']
})
export class ChangeDefaultPassComponent implements OnInit, OnDestroy {

  loading = false;
  formGroup: FormGroup;
  showPassword = false;
  showRePassword = false;
  private sub: any;

  constructor(private fb: FormBuilder,
              private encrypt: EncrdecrService,
              public router: Router,
              private route: ActivatedRoute,
              private alertController: AlertController,
              private userService: UserService) {
  }

  ngOnInit() {
    this.formGroup = this.fb.group({
      user: [''],
      oldPassword: [''],
      newPassword: ['', [Validators.required, Validators.minLength(6)]],
      rePassword: ['', [Validators.required, Validators.minLength(6)]]
    }, {
      validators: this.passwordMatchValidator
    });

    this.sub = this.route.params.subscribe(params => {
      this.formGroup.get('user').setValue(params[0]);
      this.formGroup.get('oldPassword').setValue(params[1]);
    });

  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  showPass() {
    this.showPassword = !this.showPassword;
  }

  showRePass() {
    this.showRePassword = !this.showRePassword;
  }

  passwordMatchValidator(g: FormGroup) {
    return g.get('newPassword').value === g.get('rePassword').value
      ? null : {notSame: true};
  }

  async sendData() {

    const body = {
      user: this.formGroup.get('user').value,
      oldPassword: this.encrypt.setEncrypt(this.formGroup.get('oldPassword').value),
      newPassword: this.encrypt.setEncrypt(this.formGroup.get('newPassword').value),
    };

    this.userService.getToken(body.user, this.formGroup.get('oldPassword').value).subscribe(
      (res) => {
        sessionStorage.setItem('_token', JSON.stringify(res));

        this.userService.changeDefaultPass(body).subscribe(
          (response: any) => {
            if (response.error === false) {

              this.userService.getUserInfo().subscribe(
                (loginResponse: any) => {
                  if (loginResponse.error) {
                    this.router.navigate(['login']);

                  } else {

                    this.alertController.showAlert('Info',
                      'pass-saved-message', () => {
                        loginResponse.data.uid = body.user;
                        this.userService.setUserInfo(loginResponse);
                        this.router.navigate(['profile']);
                      });
                  }
                },
                () => {
                  this.showResponseMessage();
                }
              );
            } else {
              this.showResponseMessage(response.error, response.message);
            }

          },
          (error) => {
            this.showResponseMessage();
          }
        );

      },
      (error) => {
        console.log(error);
        this.userService.logout();
      }
    );

  }

  private showResponseMessage(error?, message?) {

    this.alertController.showAlert((error) ? 'Error' : 'Info',
      (message) ? message : 'error-access-message', () => {
      });
  }
}
