import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {ChangeDefaultPassComponent} from './change-default-pass.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ChangeDefaultPassRoutingModule} from './change-default-pass-routing.module';
import {LoadingButtonModule} from '../shared/loading-button/loading-button.module';


@NgModule({
  declarations: [
    ChangeDefaultPassComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ChangeDefaultPassRoutingModule,
    TranslateModule.forChild(),
    LoadingButtonModule,
  ],
})
export class ChangeDefaultPassModule { }
