import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ProfileRoutingModule} from './profile-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {ProfileComponent} from './profile.component';
import {LoadingButtonModule} from '../shared/loading-button/loading-button.module';

@NgModule({
  declarations: [
    ProfileComponent
  ],
    imports: [
        CommonModule,
        ProfileRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule.forChild(),
        LoadingButtonModule
    ]
})
export class ProfileModule { }
