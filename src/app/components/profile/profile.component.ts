import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {UserService} from '../../providers/user.service';
import {AlertController} from '../../providers/alert-controller/alert-controller.service';
import {Router} from '@angular/router';

const BASE_64_IMAGE_PREFIX = 'data:image/jpeg;base64,';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  @ViewChild('buttonImage', {static: false}) buttonImage: ElementRef;
  loading = false;
  formGroup: FormGroup;
  userPhoto;
  hasPhoto;

  constructor(private userService: UserService,
              private fb: FormBuilder,
              private alertController: AlertController,
              public router: Router) {
  }

  ngOnInit() {
    this.hasPhoto = this.userService.getUser().data.jpegPhoto !== '';
    this.userPhoto = this.userService.getUser().data.jpegPhoto !== '' ?
      BASE_64_IMAGE_PREFIX + this.userService.getUser().data.jpegPhoto : 'assets/imgs/no-user.png';
    this.formGroup = this.fb.group({
      username: [this.userService.getUser().data.uid],
      displayName: [this.userService.getUser().data.displayName],
      email: [this.userService.getUser().data.mail],
      birthday: [this.userService.getUser().data.birthday],
      jpegPhoto: [this.userService.getUser().data.jpegPhoto],
    });
  }

  // programatically click hidden upload image button
  clickButton() {
    this.buttonImage.nativeElement.click();
  }

  handleInputChange(e) {
    const file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    const pattern = /\.?(jpe?g|png)$/i;
    const reader = new FileReader();
    if (file) { // if cancel no file selected
      if (!file.type.match(pattern)) {
        this.showResponseMessage(true, 'profile.incorrect-image-format');
      } else {
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
      }
    }

  }

  _handleReaderLoaded(e) {
    this.loading = true;
    const reader = e.target;
    this.userPhoto = reader.result;
    this.uploadProfilePhoto().then(() => {
      this.userService.getUserInfo().subscribe(
        (res: any) => {
          this.loading = false;
          if (res.error) {
            this.alertController.showAlert('Error', 'error-access-message', () => {
            });
          } else {
            this.userService.setUserInfo(res);
          }
        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );
    });
  }

  async uploadProfilePhoto(): Promise<void> {
    const body = {
      user: this.userService.getUser().data.uid,
      image: this.userPhoto
    };

    try {
      const response = await this.userService.uploadPhoto(body).toPromise();
      if (!response.error) {
        this.showResponseMessage(false, 'profile.photo-upload-ok');
      } else {
        this.showResponseMessage(true, response.message);
      }
    } catch (error) {
      this.loading = false;
      this.showResponseMessage();
    }
  }

  private showResponseMessage(error?, message?) {

    this.alertController.showAlert((error) ? 'Error' : 'Info',
      (message) ? message : 'error-access-message', () => {
      });
  }

  logout() {
    this.userService.logout(true);
  }
}
