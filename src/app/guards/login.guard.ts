import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {UserService} from '../providers/user.service';

@Injectable({
    providedIn: 'root'
})
export class LoginGuard implements CanActivate {

    constructor(private userServices: UserService, private router: Router) {
    }

    canActivate() {
        if (this.userServices.getUser() === false) {
            this.router.navigate(['login']);
            return false;
        }
        return true;
    }

}
