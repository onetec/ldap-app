import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {ErrorService} from '../providers/error.service';
import {UserService} from '../providers/user.service';

@Injectable()
export class ResponseInterceptor implements HttpInterceptor {
  constructor(private userServices: UserService, private errorServices: ErrorService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      tap((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          // console.log(event.url);
        }
      }, (err: any) => {
        if (err instanceof HttpErrorResponse) {
          switch (err.status) {
            case 404:
              console.log('Página no encontrada!');
              break;
            case 401:
              this.errorServices.addErrors(
                ['Sesión Expirada',
                  'La sesión ha expirado']
              );
              this.userServices.revokeToken().subscribe(() => {
                this.userServices.logout(true);
              }, () => this.userServices.logout(true));
              break;
            case 500:
              this.errorServices.addErrors(
                ['Error',
                  'Espere unos segundos y vuelva a intentar si el problema continua pongase en contacto con el administrador']
              );
              break;
            default:
              console.log('Error respuesta (' + err.status + '): ' + err.statusText);
              break;
          }
        }
      })
    );
  }
}
