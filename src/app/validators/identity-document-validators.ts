import {FormControl} from '@angular/forms';

const DNI_REGEX: RegExp = /^(\d{8})([a-zA-Z])$/;
const NIE_REGEX: RegExp = /^[XYZxyz]\d{7,8}[a-zA-Z]$/;

export class IdentityDocumentValidators {

  static evaluateValidDocument(c: FormControl) {
    return (IdentityDocumentValidators.validFormat(c.value)) &&
    (IdentityDocumentValidators.validDNI(c.value) || IdentityDocumentValidators.validNIE(c.value)) ? null : {pattern: true};
  }

  static validDNI(dni) {
    if (!dni || dni.length !== 9) {
      return false;
    }

    dni = dni.toUpperCase();
    const dniLetters = 'TRWAGMYFPDXBNJZSQVHLCKE';
    const letter = dniLetters.charAt(parseInt(dni.toUpperCase(), 10) % 23);

    return letter === dni.charAt(8);
  }

  public static validFormat(str) {
    return (str.match(NIE_REGEX) !== null || str.match(DNI_REGEX) !== null);
  }

  static validNIE(nie) {
    if (!nie || nie.length < 8) {
      return false;
    }
    nie = nie.toUpperCase();
    // Change the initial letter for the corresponding number and validate as DNI
    let niePrefix = nie.charAt(0);

    switch (niePrefix) {
      case 'X':
        niePrefix = 0;
        break;
      case 'Y':
        niePrefix = 1;
        break;
      case 'Z':
        niePrefix = 2;
        break;
    }

    return this.validDNI(niePrefix + nie.substr(1));

  }

}
