import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ChangePassComponent} from './components/change-pass/change-pass.component';
import {LoginComponent} from './components/login/login.component';
import {RecoverPassComponent} from './components/recover-pass/recover-pass.component';
import {LoginGuard} from './guards/login.guard';


const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'profile',
    loadChildren: './components/profile/profile.module#ProfileModule',
    canActivate: [LoginGuard]
  },
  {
    path: 'change-default-pass',
    loadChildren: './components/change-default-pass/change-default-pass.module#ChangeDefaultPassModule',
  },
  {
    path: 'recover-pass',
    component: RecoverPassComponent,
  },
  {
    path: 'change-pass',
    component: ChangePassComponent,
  },
  {
    path: '', redirectTo: 'login', pathMatch: 'full'
  },
  {
    path: '**', pathMatch: 'full', redirectTo: 'login'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
