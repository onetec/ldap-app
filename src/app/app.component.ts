import {Component, OnDestroy, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {takeUntil} from 'rxjs/operators';
import {SwUpdate} from '@angular/service-worker';
import {UserService} from './providers/user.service';
import {Subject} from 'rxjs';
import {ErrorService} from './providers/error.service';
import {Router} from '@angular/router';
import {AlertController} from './providers/alert-controller/alert-controller.service';
import {Title} from '@angular/platform-browser';
import {AnalyticsService} from './providers/analytics.service';

const TIMER_INTERVAL = 60000;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy, OnInit {

  private ngUnsubscribe = new Subject();
  timer: any = null;
  timeSessionLeft = 0;

  constructor(private errorService: ErrorService,
              private analytics: AnalyticsService,
              public router: Router,
              private userService: UserService,
              translate: TranslateService,
              private updates: SwUpdate,
              private alertController: AlertController,
              private titleService: Title
  ) {
    translate.setDefaultLang('es');
    translate.use('es');
    this.titleService.setTitle( 'GO Accounts' );
    this.initializeErrors();

    this.analytics.init();

    this.updates.available.subscribe((event) => {
      document.location.reload();
    });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  ngOnInit() {
    this.userService.tokenDurationSubject.subscribe(expiresIn => {

      if (expiresIn !== this.userService.initialTokenValue) {
        if (this.timer === null) {// only start timer if there is not another one
          this.timeSessionLeft = expiresIn;
          this.updateEveryMinute();
        } else {
          this.stopInterval();
          this.timeSessionLeft = expiresIn;
          this.updateEveryMinute();
        }
      }
    });
  }

  private stopInterval() {
    clearInterval(this.timer);
    this.timer = null;
  }

  private updateEveryMinute() {
    this.timer = setInterval(() => {
      this.timeSessionLeft = this.timeSessionLeft - 60;
      // console.log('timeSessionLeft ' + this.timeSessionLeft);
      if (this.timeSessionLeft <= 0) {
        // console.log('sessionExpired ');
        this.stopInterval();
        this.userService.logout(true);
      }
    }, TIMER_INTERVAL);
  }

  private initializeErrors() {
    this
      .errorService
      .getErrors()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((errors) => {
        this.alertController.showAlert(
          errors[0],
          errors[1],
          () => {
          }
        );
      });
  }
}
